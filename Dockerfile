FROM nextcloud:13.0.4

RUN mkdir -p /srv/data \
 && chown www-data:www-data /srv/data