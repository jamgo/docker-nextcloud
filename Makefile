NAME = jamgocoop/nextcloud
VERSION = 13.0.4

.PHONY: build build-nocache push push-latest release git-tag-version

build:
#	docker build -t $(NAME):$(VERSION) --rm .
	docker build -t $(NAME):$(VERSION) .

build-nocache:
	docker build -t $(NAME):$(VERSION) --no-cache --rm .

push:
	docker push $(NAME):$(VERSION)

push-latest:
	docker push $(NAME):latest

release: build push push-latest

git-tag-version: release
	git tag -a v$(VERSION) -m "v$(VERSION)"
	git push origin v$(VERSION)
